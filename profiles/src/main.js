import Vue from 'vue'
import vuetify from './plugins/vuetify'
import Vuex from 'vuex'
import VueRouter from 'vue-router'

import App from './App.vue'
import axios from 'axios'


/* Middlewares */
Vue.use(Vuex)
Vue.use(VueRouter)

Vue.config.productionTip = false



/* ROUTER */

import MainComponent from './components/MainComponent.vue';
import UserComponent from './components/UserComponent.vue';


const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: MainComponent},
    { path: '/user', component: UserComponent },
  ]
})



/* STORE */

const store = new Vuex.Store({
  state: {
    count: 0,
    users: [],
    user: [],
    errorUsers: "",
    load: false

  },
  getters: {
    usersTable: state => {
      var array = []
      state.users.map(user => {
          var dob = user.dob.date.slice(0, 10); 
          var registered = user.registered.date.slice(0,10)

          array.push({
            gender: user.gender,
            name: user.name.last + " " + user.name.first,
            email: user.email,
            location: user.location.country,
            dob: dob,
            registered: registered
          })
      })
      return array
    },

    userProfile: state => {

      var obj = {}
      var user = state.user

      obj = {
          picture: user.picture.large,
          location: user.location,
          favourite: null,
          name: user.name.last + " " + user.name.first,
          phone: user.phone,
          email: user.email
      }

      console.log(obj);
      return obj

    }
  },
  mutations: {
    increment (state) {
      state.count++
    },
    setUsers(state, users){
        state.users = users
    },
    setError(state, err){
        state.errorUsers = err
    },
    setLoading(state){
        state.load = !state.load
    },
    setUser(state, index){
        state.user = index
    }

  },
  actions: {

    loadUsers({commit}){
      // commit('setLoading')
      axios
        .get('http://localhost:5000/api/users')
        .then(response => response.data)
        .then(users => {
            commit('setUsers', users)
            // commit('setLoading')
        })
        .catch(err => commit('setError', err));
    },
    selectedUser({commit}, {index}){
  
        commit('setUser', store.state.users[index])

    }
  }
})


/* VUE INSTANCE */

new Vue({
  vuetify,
  router,
  store: store,
  render: h => h(App)
}).$mount('#app')
