const express = require('express')
const axios = require('axios')
const router = express.Router()


const url_base = 'https://randomuser.me/api/'

// Obtengo los usuarios
router.get('/', (req, res) => {
    
    axios(url_base+'?results=100').then(resp => {
        if(resp.data.results){

            // Envio todos los datos
            res.send(resp.data.results)
        }
    })
});

// Rutas microservicios (segunda parte del proyecto)

module.exports = router;
